var Trackers = {};
var Profile = 'untitled'
var Profiles = [];
var row = 0;
var column = 0;

function AddTracker(Name, Tracker, header, money, addition, subtraction, change, del, ID, MyMoney) {

if(MyMoney == null) {
  MyMoney = 0;

}
  if (Name == null) {
    Name = prompt('What would you like to call this tracker?')
    Trackers[Name] = '0';
  }

  Tracker = document.createElement('div');
  header = document.createElement('h1');
  money = document.createElement('h2');
  addition = document.createElement('BUTTON');
  subtraction = document.createElement('BUTTON');
  change = document.createElement('BUTTON');
  del = document.createElement('BUTTON');
  if(column == 0) {
var left =row*240 + 30;
var top = column*126.5 + 230;
}else{
  var left =row*240 + 30;
  var top = column*126.5 + 253;
}
  Tracker.setAttribute("id", Name);
  Tracker.setAttribute("name", "Tracker");
  Tracker.setAttribute("style", "position:fixed; width: 236px;left:" + left + "px;top:"+top +"px;border:solid");

  document.getElementById('trackers').appendChild(Tracker);
  Tracker.appendChild(header);
  Tracker.appendChild(money);
  Tracker.appendChild(addition);
  Tracker.appendChild(subtraction);
  Tracker.appendChild(change);
  Tracker.appendChild(del);
  header.innerHTML = Name;
  del.innerHTML = "Delete";
  money.innerHTML = '$' + MyMoney;
  addition.innerHTML = '+10';
  subtraction.innerHTML = '-10';
  change.innerHTML = 'Change Amount';
  addition.setAttribute("onClick", "addition(this.parentElement.id);");
  subtraction.setAttribute("onClick", "subtraction(this.parentElement.id);");
  change.setAttribute("onClick", "set(this.parentElement.id);");
  del.setAttribute("onClick", "this.parentElement.remove(); delete Trackers[this.parentElement.id]; Save();Adjust();load(Profile)")
  Save()
  if(row == 3) {
    row = 0;
    column++
  }else{
    row++;

  }

}

function addition(name) {

  Trackers[name] = parseInt(Trackers[name]) + 10;
  document.getElementById(name).childNodes[1].innerHTML = '$' + Trackers[name];
  Save();

}

function subtraction(name) {

  Trackers[name] = parseInt(Trackers[name]) - 10;
  document.getElementById(name).childNodes[1].innerHTML = '$' + Trackers[name];
  Save();

}

function set(name, money) {

  money = prompt('What would you like to set your tracker to?');
  Trackers[name] = money;
  document.getElementById(name).childNodes[1].innerHTML = '$' + Trackers[name];
  Save();


}


function Create(name) {
  Profiles.push(name);
  localStorage.setItem('Profiles', Profiles);
  Profile = name;
  document.getElementById('name').innerHTML = Profile;
  ListAdd(name);

}

function ListAdd(name) {

  var ul = document.getElementById('child-list');
  var li = document.createElement('li');
  li.innerHTML = '<input type="radio" name="list" id="'+name+'"onclick="if(this.checked == true) {load(this.value)}" value="' + name + '">' + name + '&nbsp <BUTTON onclick = "localStorage.removeItem(\''+ name +'\');Profiles.splice(Profiles.indexOf(\''+name+'\'),1);localStorage.setItem(\'Profiles\',Profiles); location.reload();">Delete';
  ul.appendChild(li);

}

function load(name) {
  row = 0;
  column = 0;
  Profile = name;
  document.getElementById('trackers').innerHTML = '';

if(localStorage.getItem(name) != null) {
  Trackers = JSON.parse(localStorage.getItem(name));
}else{
  Trackers = {};
}


  for (trackerName in Trackers) {
    AddTracker(trackerName, null, null, null, null, null, null, null, null, Trackers[trackerName])
  }

  document.getElementById('name').innerHTML = Profile;

}

function Save() {
var stringified = JSON.stringify(Trackers);
  localStorage.setItem(Profile, stringified);

}

function startup() {
  Profiles = localStorage.getItem('Profiles').split(',');

  for (i = 0; i < Profiles.length; i++) {
if(Profiles[i] != null && Profiles[i] != "") {
    ListAdd(Profiles[i]);
  }
  }



}
function Adjust () {

    row -= 1;

    if(column !=0) {
      column -= 1
    }

if(row < 0) {
  row = 3;
}
}
